<? require("db.php"); ?>

<!DOCTYPE html>
<html>
<head>
    <base href="//shop.chaosdidi.de"></base>
    <!-- ==========================
        Meta Tags 
    =========================== -->
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    
    <!-- ==========================
        Title 
    =========================== -->
    <title>ChaosDidi</title>
        
    <!-- ==========================
        Fonts 
    =========================== -->
    <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,200italic,300,300italic,400italic,600,600italic,700,700italic,900&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <!-- ==========================
        CSS 
    =========================== -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/animate.css" rel="stylesheet" type="text/css">
    <link href="assets/css/owl.carousel.css" rel="stylesheet" type="text/css">
    <link href="assets/css/owl.theme.css" rel="stylesheet" type="text/css">
    <link href="assets/css/owl.transitions.css" rel="stylesheet" type="text/css">
    <link href="assets/css/creative-brands.css" rel="stylesheet" type="text/css">
    <link href="assets/css/jquery.vegas.min.css" rel="stylesheet" type="text/css">
    <link href="assets/css/magnific-popup.css" rel="stylesheet" type="text/css">
    <link href="assets/css/custom.css" rel="stylesheet" type="text/css">
    
    <!-- ==========================
        JS 
    =========================== -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
    
</head>
<body>
    <h1>ChaosDidi</h1>
	
    <!-- HEADER - START  -->
    <? require("assets/inc/topbar.php"); ?>
    <!-- HEADER - END  -->  
    

    <!-- TITLE - START  -->
    <div class="container hidden-xs">
        <div class="header-title">
            <div class="pull-left">
                <h2><a href="./"><span class="text-primary">Chaos</span>Didi</a></h2>
                <p>&nbsp;</p>
            </div>
        </div>
    </div>
    <!-- TITLE - END  -->
            
    
    <!-- CONTENT - START  -->
    <div class="container">
        <section class="content-wrapper">
        	<div class="box">
                            
                <!-- ERROR - START -->
                <div class="error">
                	<h2>Fehler 404 - Seite nicht gefunden</h2>
                	<p>
                        Hmm.. irgendetwas muss schief gelaufen sein :O<br>
                        Ich hoffe mal, dass du einfach nur einen falschen Link hattest, ansonsten meld dich gerne einmal beim <a href="mailto:admin@chaosdidi.de">Admin</a> ^-^'
                    </p>
                	<a href="./" class="btn btn-primary btn-lg">Zurück zur Startseite</a>
                </div>
                <!-- ERROR - END -->
                
            </div>
        </section>
    </div>
    <!-- CONTENT - END  -->

   
    <!-- FOOTER - START  -->
    <? require("assets/inc/footer.php"); ?>
    <!-- FOOTER - END  -->

   
   	<!-- JS  -->        
	<script src="http://code.jquery.com/jquery-latest.min.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
    <script src="assets/js/owl.carousel.js"></script>
    <script src="assets/js/jquery.magnific-popup.min.js"></script>
    <script src="assets/js/creative-brands.js"></script>
    <script src="assets/js/jquery.vegas.min.js"></script>
    
    <script src="assets/js/INDEX-custom.js"></script>
</body>
</html>