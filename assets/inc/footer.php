<div class="container">
	<div class="footer-boxes">
    	<div class="row">
                    
            <!-- FOOTER ABOUT US - START -->
        	<div class="col-md-4 col-md-offset-4 hidden-xs hidden-sm">
            	<div class="box">
                	<h4>Über uns</h4>
                    <p>
                        Deutscher Let's Player mit leichtem Andrang zu Schizophrenie und Persönlichkeitsspaltungen.<br>
                        Meist PS3 Spiele, aber auf Wunsch auch PC.<br>
                        Enjoy it ^^
                    </p>
                </div>
        	</div>
            <!-- FOOTER ABOUT US - END -->
            
        </div>
    </div>
	<footer class="navbar navbar-default">
    	<div class="row">
        	<div class="col-md-6 hidden-xs hidden-sm">
            	<ul class="nav navbar-nav">
                    <li><a href="./">Startseite</a></li>
                    <li><a href="Bug">Fehler gefunden?</a></li>
                    <li><a href="Imprint">Impressum</a></li>
                    <li><a href="Dpd">Datenschutzerklärung</a></li>
                </ul>
            </div>
            <div class="col-md-6">
            	<p class="copyright">© ChaosDidi 2014 - <? echo date("Y"); ?> Alle Rechte vorbehalten. Design von <a href="http://pixelized.cz/" target="_blank">Pixelized Studio.</a></p>
            </div>
        </div>
    </footer>
</div>