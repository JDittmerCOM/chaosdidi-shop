<div class="box">
	<h2>Impressum</h2>
	<h4><b>Angaben gemäß § 5 TMG:</b></h4><br>

	<div class="row">
		<div class="col-md-6">
			<b>Let's Player, Administrator</b><br>
		    Kevin Dittmer<br>
		    Friesenstr. 20<br>
		    21680 Stade<br><br>

		    <h4><b>Kontakt:</b></h4>
		    Telefon: <b>auf Anfrage</b><br>
		    E-Mail: <a href="mailto:kd@chaosdidi.de">kd@chaosdidi.de</a>
		</div>
		<div class="col-md-6">
			<b>Seitenbetreiber, Webmaster, Administrator</b><br>
		    Justin Dittmer<br>
		    Dangaster Weg 4<br>
		    21129 Hamburg<br><br>

		    <h4><b>Kontakt:</b></h4>
		    Telefon: +49 (0) 162 687 1189<br>
		    E-Mail: <a href="mailto:jd@chaosdidi.de">jd@chaosdidi.de</a>
		</div>
	</div>
	<br>
    Quelle: <a href="http://www.e-recht24.de" target="_blank">http://www.e-recht24.de</a>
	<br><br>


	Aktuelle Shop.ChaosDidi.de Version: <? require("VERSION.php"); ?>
    <hr/>
	<h4>Haftungsausschluss (Disclaimer)</h4>
	<b>Haftung für Inhalte</b><br>
	Als Diensteanbieter sind wir gemäß § 7 Abs.1 TMG für eigene Inhalte auf diesen Seiten nach den allgemeinen Gesetzen verantwortlich. Nach §§ 8 bis 10 TMG sind wir als Diensteanbieter jedoch nicht verpflichtet, übermittelte oder gespeicherte fremde Informationen zu überwachen oder nach Umständen zu forschen, die auf eine rechtswidrige Tätigkeit hinweisen. Verpflichtungen zur Entfernung oder Sperrung der Nutzung von Informationen nach den allgemeinen Gesetzen bleiben hiervon unberührt. Eine diesbezügliche Haftung ist jedoch erst ab dem Zeitpunkt der Kenntnis einer konkreten Rechtsverletzung möglich. Bei Bekanntwerden von entsprechenden Rechtsverletzungen werden wir diese Inhalte umgehend entfernen.
	<br><br>

	<b>Haftung für Links</b><br>
	Unser Angebot enthält Links zu externen Webseiten Dritter, auf deren Inhalte wir keinen Einfluss haben. Deshalb können wir für diese fremden Inhalte auch keine Gewähr übernehmen. Für die Inhalte der verlinkten Seiten ist stets der jeweilige Anbieter oder Betreiber der Seiten verantwortlich. Die verlinkten Seiten wurden zum Zeitpunkt der Verlinkung auf mögliche Rechtsverstöße überprüft. Rechtswidrige Inhalte waren zum Zeitpunkt der Verlinkung nicht erkennbar. Eine permanente inhaltliche Kontrolle der verlinkten Seiten ist jedoch ohne konkrete Anhaltspunkte einer Rechtsverletzung nicht zumutbar. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Links umgehend entfernen.
	<br><br>

	<b>Urheberrecht</b><br>
	Die durch die Seitenbetreiber erstellten Inhalte und Werke auf diesen Seiten unterliegen dem deutschen Urheberrecht. Die Vervielfältigung, Bearbeitung, Verbreitung und jede Art der Verwertung außerhalb der Grenzen des Urheberrechtes bedürfen der schriftlichen Zustimmung des jeweiligen Autors bzw. Erstellers. Downloads und Kopien dieser Seite sind nur für den privaten, nicht kommerziellen Gebrauch gestattet. Soweit die Inhalte auf dieser Seite nicht vom Betreiber erstellt wurden, werden die Urheberrechte Dritter beachtet. Insbesondere werden Inhalte Dritter als solche gekennzeichnet. Sollten Sie trotzdem auf eine Urheberrechtsverletzung aufmerksam werden, bitten wir um einen entsprechenden Hinweis. Bei Bekanntwerden von Rechtsverletzungen werden wir derartige Inhalte umgehend entfernen.
</div>