<?
    $steam_keys_num = mysqli_num_rows(mysqli_query($db_shop, "SELECT * FROM steam_keys"));
    $steam_keys_sold_num = mysqli_num_rows(mysqli_query($db_shop, "SELECT * FROM steam_keys_sold"));
	$domains_sold_num = mysqli_num_rows(mysqli_query($db_shop, "SELECT * FROM domains_sold"));
?>

<div class="box">
	<h2>Administratoren-Bereich</h2>
	<div id="tournament-groups">
		<table class="table table-bordered">
            <thead>
                <tr>
                    <th colspan="3">Alles im Überblick</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                	<td class="text-center"><i class="fa fa-check fa-fw"></i></td>
                	<td class="text-center"><? echo $steam_keys_num ?></td>
                	<td><a href="Admin/SteamKeys/Availabel">Verfügbare Steam-Keys</a></td>
                </tr>
                <tr>
                	<td class="text-center"><i class="fa fa-money fa-fw"></i></td>
                	<td class="text-center"><? echo $steam_keys_sold_num ?></td>
                	<td><a href="Admin/SteamKeys/Sold">Verkaufte Steam-Keys</a></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                	<td class="text-center"><i class="fa fa-money fa-fw"></i></td>
                	<td class="text-center"><? echo $domains_sold_num ?></td>
                	<td><a href="Admin/Domains/Sold">Verkaufte Domains</a></td>
                </tr>
            </tbody>
        </table>
	</div>
</div>