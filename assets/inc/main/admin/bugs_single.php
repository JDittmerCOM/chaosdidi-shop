<div class="box">
	<h2><? echo "<a href='Admin/Bugs/All'>Alle Fehlermeldungen</a> <i class='fa fa-arrow-right fa-fw'></i> Bug (#".$_GET['id'].")"; ?></h2>

	<?
		$bugID = $_GET['id'];
		$bug_sql = mysqli_query($db, "SELECT * FROM bugs WHERE id = '".$bugID."'");
		$bug_row = mysqli_fetch_assoc($bug_sql);

		if(isset($_POST['del_bug'])){
			mysqli_query($db, "DELETE FROM bugs WHERE id = '".$bugID."'");
			if(mysql_error()){exit(mysql_error());}
			header("Location:".SERVER_NAME."Admin/Bugs/All");
		}

		if($bug_row['status'] == 0){$status = "<div class='col-md-8 bg-info'><strong>Wartet auf Annahme</strong></div>";}
		if($bug_row['status'] == 1){$status = "<div class='col-md-8 bg-warning'><strong>In Überprüfung</strong></div>";}
		if($bug_row['status'] == 2){$status = "<div class='col-md-8 bg-success'><strong>Wird bearbeitet von JND_3004</strong></div>";}

		echo "<div class='row'>";
			echo "<div class='col-md-8'>";
				echo "<div class='row'>";
					echo "<div class='col-md-4 text-right'><strong>Status:</strong></div>";
					echo $status;
				echo "</div>";

				echo "<br>";

				echo "<div class='row'>";
					echo "<div class='col-md-4 text-right'><strong>IP-Adresse:</strong></div>";
					echo "<div class='col-md-8'>".$bug_row['ip']."</div>";
				echo "</div>";
				echo "<div class='row'>";
					echo "<div class='col-md-4 text-right'><strong>Datum:</strong></div>";
					echo "<div class='col-md-8'>".date("d.m.Y - H:i:s", $bug_row['date'])." Uhr</div>";
				echo "</div>";
				echo "<div class='row'>";
					echo "<div class='col-md-4 text-right'><strong>Benutzername:</strong></div>";
					echo "<div class='col-md-8'>".$bug_row['username']."</div>";
				echo "</div>";

				echo "<br>";

				echo "<div class='row'>";
					echo "<div class='col-md-4 text-right'><strong>Betreff:</strong></div>";
					echo "<div class='col-md-8'>".$bug_row['subject']."</div>";
				echo "</div>";
				echo "<div class='row'>";
					echo "<div class='col-md-4 text-right'><strong>Fehlerbeschreibung:</strong></div>";
					echo "<div class='col-md-8'>".str_replace("\n", "<br>", $bug_row['message'])."</div>";
				echo "</div>";
			echo "</div>";
			echo "<div class='col-md-4'>";
				echo "<form method='post'>";
					echo "<button type='submit' name='del_bug' class='btn btn-danger btn-block'>Löschen</button>";
				echo "</form>";
			echo "</div>";
		echo "</div>";
	?>
</div>