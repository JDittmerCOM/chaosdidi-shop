<div class="box">
	<h2>Alle Fehlermeldungen</h2>
	<div id="tournament-groups">
		<table class="table table-bordered">
			<thead>
				<tr>
					<th></th>
					<th>Datum</th>
					<th>Benutzername</th>
					<th>Betreff</th>
				</tr>
			</thead>
			<tbody>
				<?
					$bugs_sql = mysqli_query($db, "SELECT * FROM bugs ORDER BY id DESC");
					while($row = mysqli_fetch_assoc($bugs_sql)){
						if($row['username'] == "Gast"){$username = "Gast";}else{$username = "<a href='MyProfile/".$row['userID']."' target='_blank'>".$row['username']."</a>";}
						echo "<tr>";
							echo "<td><a href='Admin/Bugs/Single/".$row['id']."' class='btn btn-success btn-block'><i class='fa fa-external-link-square fa-fw'></i></a></td>";
							echo "<td>".date("d.m.Y - H:i", $row['date'])." Uhr</td>";
							echo "<td>".$username."</td>";
							echo "<td>".$row['subject']."</td>";
						echo "</tr>";
					}
				?>
			</tbody>
		</table>
	</div>
</div>