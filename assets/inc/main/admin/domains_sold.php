<div class="box">
	<h2>Verkaufte Domains</h2>
	<div id="tournament-groups">
		<table class="table table-bordered">
            <thead>
                <tr>
                    <th class="text-center">#</th>
                    <th>Order ID</th>
                    <th>Datum</th>
                    <th>Benutzername</th>
                    <th>Artikelname</th>
                </tr>
            </thead>
            <tbody>
        	<?
        		$sql = mysqli_query($db_shop, "SELECT * FROM domains_sold ORDER BY id ASC");
        		while($row = mysqli_fetch_assoc($sql)){
        			echo "<tr>";
        				echo "<td class='text-center'>".$row['id']."</td>";
        				echo "<td><a href='Admin/Domains/OrderID/".$row['order_id']."'>".$row['order_id']."</a></td>";
        				echo "<td>".date("d.m.Y H:i", $row['date'])."</td>";
        				echo "<td>".$row['username']."</td>";
        				echo "<td>".$row['article_name']."</td>";
        			echo "</tr>";
        		}
        	?>
            </tbody>
        </table>
	</div>
</div>