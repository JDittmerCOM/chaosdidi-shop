<div class="box">
	<h2>Kontostand (<? echo CREDITS." Pkt."; ?>) <i class="fa fa-arrow-right fa-fw"></i> Spenden</h2>
	<br>
	<?
		if(isset($_POST['sub_transfer'])){
			$points = mysqli_real_escape_string($db, $_POST['points']);
			$username = mysqli_real_escape_string($db, $_POST['username']);

			$sql = mysqli_query($db, "SELECT * FROM users WHERE username = '".$username."'");
			$row = mysqli_fetch_assoc($sql);

			if($username == $row['username'] && $row['status'] == 2){
				if(preg_match("/^[0-9]*$/", $points)){
					if($username != USERNAME){
						if((CREDITS - $points) >= 0){
							// Punkte vom eigenen Konto abziehen
							mysqli_query($db, "UPDATE users SET credits = (credits - ".$points.") WHERE id = '".ID."'");
							if(mysql_error()){exit(mysql_error());}

							// Punkte auf Konto des Empfängers buchen
							mysqli_query($db, "UPDATE users SET credits = (credits + ".$points.") WHERE id = '".udaUSERNAME($username, "id")."'");
							if(mysql_error()){exit(mysql_error());}

							// Kontoauszug für Sender erstellen
							mysqli_query($db, "INSERT INTO balance_statements (`date`, 
																			   userID, 
																			   description, 
																			   balance) 
													VALUES ('".time()."', 
															'".ID."', 
															'Punkte an <u>".$username."</u> gesendet', 
															'-".$points."')
							");
							if(mysql_error()){exit(mysql_error());}

							// Kontoauszug für Empfänger erstellen
							mysqli_query($db, "INSERT INTO balance_statements (`date`, 
																			   userID, 
																			   description, 
																			   balance) 
													VALUES ('".time()."', 
															'".udaUSERNAME($username, "id")."', 
															'Punkte von <u>".USERNAME."</u> erhalten', 
															'".$points."')
							");
							if(mysql_error()){exit(mysql_error());}

							// Benachrichtigung für Empfänger erstellen
							$notification = "Du hast von <u>".USERNAME."</u> soeben ".$points." Punkte erhalten.";
							mysqli_query($db, "INSERT INTO users_notifications (`date`, userID, notification) VALUES ('".time()."', '".udaUSERNAME($username, "id")."', '".$notification."')");
							if(mysql_error()){exit(mysql_error());}

							// eMail an Empfänger senden
							if(udaUSERNAME($username, "noti_new_noti") == 1){
								$mail_to = udaUSERNAME($username, "email");
						        $mail_subject = "[ChaosDidi] Neue Benachrichtigung";
						        $mail_message = '
						        <html>
						            <head>
						                <title>[ChaosDidi] Neue Benachrichtigung</title>
						                <link href="http://chaosdidi.de/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
						            </head>
						            <body>
						                <br>
						                <div class="container">
						                	<img src="http://chaosdidi.de/assets/images/logo.png" class="img-responsive">
						                	<br><br>
						                    Du hast heute <strong>am '.date("d.m.Y").' um '.date("H:i:s").' Uhr</strong> auf <strong>ChaosDidi.de</strong> eine neue Benachrichtigung erhalten.
						                    <hr/>
						                    <div class="row">
						                        <div class="col-md-3 text-right"><strong>Benachrichtigung:</strong></div>
						                        <div class="col-md-9">'.$notification.'</div>
						                    </div>
						                    <hr/>
						                    <a href="http://chaosdidi.de/Notifications">http://chaosdidi.de/Notifications</a>
						                	<br><br>
						                </div>
						            </body>
						        </html>
						        ';

						        // Always set content-type when sending HTML email
						        $mail_headers = "MIME-Version: 1.0" . "\n";
						        $mail_headers .= "Content-type:text/html;charset=UTF-8" . "\n";
						        $mail_headers .= 'From: ChaosDidi <no-reply@chaosdidi.de>' . "\n";

						        mail($mail_to,$mail_subject,$mail_message,$mail_headers);
						    }

							header("Location:".SERVER_NAME."Balance/Donations");
						}else{
							echo bad("Du hast <strong>nicht genügend Punkte</strong> zum Transferieren!");
						}
					}else{
						echo bad("Du kannst dir <strong>selber keine Punkte</strong> zukommen lassen!");
					}
				}else{
					echo bad("Die <strong>Punkteeingabe</strong> ist <strong><u>ungültig</u></strong>!");
				}
			}else{
				echo bad("Dieser <strong>Benutzername existiert nicht</strong> oder dessen <strong>Benutzerkonto ist gesperrt</strong>!");
			}
		}
	?>
	<form method="post">
		<div class="row">
			<div class="col-md-4">
				<div class="form-group">
					<label>Punkte zum Spenden</label>
					<div class="input-group">
		      			<div class="input-group-addon">Pkt.</div>
		      			<input type="text" class="form-control" name="points" placeholder="500" required>
		      		</div>
		      	</div>
			</div>
			<div class="col-md-1 text-center">
				<div class="form-group">
					<i class="fa fa-long-arrow-right fa-3x fa-fw donation-arrow-line-height"></i>
				</div>
			</div>
			<div class="col-md-5">
				<div class="form-group">
					<label>an Benutzername</label>
					<div class="input-group">
		      			<div class="input-group-addon"><i class="fa fa-user fa-fw"></i></div>
	      				<input type="text" class="form-control" name="username" placeholder="MaxMuster" required>
	      			</div>
	      		</div>
			</div>
			<div class="col-md-2">
				<div class="form-group">
					<label>&nbsp;</label>
					<button type="submit" class="btn btn-inverse btn-block" name="sub_transfer"><i class="fa fa-send fa-fw"></i></button>
				</div>
			</div>
		</div>
	</form>
</div>