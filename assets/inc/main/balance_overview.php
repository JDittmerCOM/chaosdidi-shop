<div class="box">
	<h2>Kontostand (<? echo CREDITS." Pkt."; ?>) <i class="fa fa-arrow-right fa-fw"></i> Übersicht</h2>
	<br>
	<h2><u>Wie kann ich Punkte verdienen?</u></h2>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th width="75%">Beschreibung</th>
				<th width="25%">Punkte</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Einmal am Tag einloggen</td>
				<td>10</td>
			</tr>
			<tr>
				<td>An einer Umfrage teilnehmen</td>
				<td>10</td>
			</tr>
			<tr>
				<td>Benutzer Empfehlung<br><? echo "http://chaosdidi.de?ref=".ID; ?></td>
				<td>15 <small>(Nur bei einer erfolgreichen Registrierung)</small></td>
			</tr>
		</tbody>
	</table>
</div>