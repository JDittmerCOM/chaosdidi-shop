<div class="box">
	<h2>Steam-Keys</h2>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>Art. Nr.</th>
				<th>Name</th>
				<th>Beschreibung</th>
				<th>Preis</th>
				<th></th>
			</tr>
		</thead>
		<tbody>
		<?
			$sql = mysqli_query($db_shop, "SELECT * FROM steam_keys ORDER BY article_name ASC");
			while($row = mysqli_fetch_assoc($sql)){
				$game_informations_sql = mysqli_query($db, "SELECT * FROM raffle_keys WHERE game_name = '".$row['article_name']."'");
				$game_informations_row = mysqli_fetch_assoc($game_informations_sql);

				if(CREDITS < $row['article_price']){
					$article_price = "<td class='danger'>".$row['article_price']." Pkt.</td>";
					$buy_btn = "<td><button type='button' class='btn btn-success btn-xs btn-block' disabled>Kaufen</button></td>";
				}else{
					$article_price = "<td>".$row['article_price']." Pkt.</td>";
					$buy_btn = "<td><button type='button' class='btn btn-success btn-xs btn-block' data-toggle='modal' data-target='#buy".$row['id']."'>Kaufen</button></td>";
				}

				if(isset($_POST['buyKey_'.$row['id'].'_sub'])){
					$order_id = rand(100,399)."-".rand(1000000,9999999)."-".rand(1000000,9999999);
					$date = time();
					$db_article_name = $row['article_name'];
					$db_article_price = $row['article_price'];
					$username = USERNAME;
					$birth_d = mysqli_real_escape_string($db_shop, $_POST['birth_d']);
					$birth_m = mysqli_real_escape_string($db_shop, $_POST['birth_m']);
					$birth_y = mysqli_real_escape_string($db_shop, $_POST['birth_y']);
					$birthday = $birth_d.". ".$birth_m." ".$birth_y;

					mysqli_query($db_shop, "INSERT INTO steam_keys_sold (order_id,
																		 `date`, 
																		 article_name, 
																		 article_price, 
																		 username, 
																		 birthday) 
												VALUES ('".$order_id."',
														'".$date."', 
														'".$db_article_name."', 
														'".$db_article_price."', 
														'".$username."', 
														'".$birthday."')
					");
					if(mysql_error()){exit(mysql_error());}

					mysqli_query($db, "INSERT INTO balance_statements (`date`, 
															   userID, 
															   description, 
															   balance) 
									VALUES ('".time()."', 
											'".ID."', 
											'Shop - Steam-Key für <u>".$row['article_name']."</u> gekauft', 
											'-".$row['article_price']."')
					");
					if(mysql_error()){exit(mysql_error());}

					mysqli_query($db, "UPDATE users SET credits = (credits - ".$row['article_price'].") WHERE id = '".ID."'");
					if(mysql_error()){exit(mysql_error());}

					$to1 = EMAIL;
					$subject1 = "Deine Bestellung auf ChaosDidi.de";
					$message1 = '
					<html>
						<head>
							<title>Deine Bestellung auf ChaosDidi.de</title>
							<link href="http://chaosdidi.de/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
						</head>
						<body>
							<br>
							<div class="container">
								<img src="http://chaosdidi.de/assets/images/logo.png" class="img-responsive">
								<br><br>
								Vielen Dank für deine Bestellung auf ChaosDidi.de :)
								<br><br>
								<strong>Einzelheiten deiner Bestellung</strong>
								<hr/>
								<div class="row">
									<div class="col-xs-3 text-right"><strong>Bestellung:</strong></div>
									<div class="col-xs-9">#'.$order_id.'</div>
								</div>
								<div class="row">
									<div class="col-xs-3 text-right"><strong>Bestellt am:</strong></div>
									<div class="col-xs-9">'.date("d.m.Y H:i:s", $date).' Uhr</div>
								</div>
								<br>
								<div class="row">
									<div class="col-xs-3 text-right"><strong>Spielname:</strong></div>
									<div class="col-xs-9">'.$row['article_name'].'</div>
								</div>
								<div class="row">
									<div class="col-xs-3 text-right"><strong>USK:</strong></div>
									<div class="col-xs-9">'.$game_informations_row['game_usk'].'</div>
								</div>
								<div class="row">
									<div class="col-xs-3 text-right"><strong>Betriebssystem/e:</strong></div>
									<div class="col-xs-9">'.$game_informations_row['bs'].'</div>
								</div>
								<div class="row">
									<div class="col-xs-3 text-right"><strong>Preis:</strong></div>
									<div class="col-xs-9">'.$row['article_price'].' Pkt.</div>
								</div>
								<div class="row">
									<div class="col-xs-3 text-right"><strong>Key:</strong></div>
									<div class="col-xs-9"><a href="'.$game_informations_row['game_key'].'">'.$game_informations_row['game_key'].'</a></div>
								</div>
								<hr/>
								Viel Spaß wünscht dir,<br>
								Das ChaosDidi Team<br><br>
							</div>
						</body>
					</html>
					';

					// Always set content-type when sending HTML email
					$headers1 = "MIME-Version: 1.0" . "\n";
					$headers1 .= "Content-type:text/html;charset=UTF-8" . "\n";
					$headers1 .= 'From: ChaosDidi <shop@chaosdidi.de>' . "\n";

					mail($to1,$subject1,$message1,$headers1);

					mysqli_query($db_shop, "DELETE FROM steam_keys WHERE id = '".$row['id']."'");
					if(mysql_error()){exit(mysql_error());}

					header("Location:".SERVER_NAME."Buy/SteamKeys");
				}

				echo "<tr>";
					echo "<td>".$row['id']."</td>";
					echo "<td>".$row['article_name']."</td>";
					echo "<td><a href='#' data-toggle='modal' data-target='#info".$row['id']."'>Informationsfenster</a></td>";
					echo $article_price;
					echo $buy_btn;
				echo "</tr>";

				echo "<div class='modal fade' id='info".$row['id']."' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>";
					echo "<div class='modal-dialog' role='document'>";
						echo "<div class='modal-content'>";
							echo "<div class='modal-header'>";
								echo "<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>";
								echo "<h4 class='modal-title' id='myModalLabel'>Informationen über &quot;".$row['article_name']."&quot;</h4>";
							echo "</div>";
							echo "<div class='modal-body'>";
								echo "<div class='row'>";
									echo "<div class='col-md-5 text-right'><strong>Spielname:</strong></div>";
									echo "<div class='col-md-7'>".$game_informations_row['game_name']."</div>";
								echo "</div>";
								echo "<div class='row'>";
									echo "<div class='col-md-5 text-right'><strong>Steam Preis:</strong></div>";
									echo "<div class='col-md-7'>".$game_informations_row['game_price']." €</div>";
								echo "</div>";
								echo "<div class='row'>";
									echo "<div class='col-md-5 text-right'><strong>USK:</strong></div>";
									echo "<div class='col-md-7'>".$game_informations_row['game_usk']."</div>";
								echo "</div>";
								echo "<div class='row'>";
									echo "<div class='col-md-5 text-right'><strong>Betriebssystem:</strong></div>";
									echo "<div class='col-md-7'>".$game_informations_row['bs']."</div>";
								echo "</div>";
								echo "<br>";
								echo "<div class='row'>";
									echo "<div class='col-md-5 text-right'><strong>Genre:</strong></div>";
									echo "<div class='col-md-7'>".$game_informations_row['game_genre']."</div>";
								echo "</div>";
								echo "<div class='row'>";
									echo "<div class='col-md-5 text-right'><strong>Entwickler:</strong></div>";
									echo "<div class='col-md-7'>".$game_informations_row['game_developer']."</div>";
								echo "</div>";
								echo "<div class='row'>";
									echo "<div class='col-md-5 text-right'><strong>Publisher:</strong></div>";
									echo "<div class='col-md-7'>".$game_informations_row['game_publisher']."</div>";
								echo "</div>";
								echo "<br>";
								echo "<div class='row'>";
									echo "<div class='col-md-5 text-right'><strong>Veröffentlichung:</strong></div>";
									echo "<div class='col-md-7'>".$game_informations_row['game_publishing']."</div>";
								echo "</div>";
								if(strlen($game_informations_row['game_more_info']) > 0){
									echo "<br>";
									echo "<div class='row'>";
										echo "<div class='col-md-5 text-right'><strong>Weitere Informationen:</strong></div>";
										$game_more_info = preg_replace("/(sub|app):([0-9]{1,6})/", "<a href='http://store.steampowered.com/$1/$2/' target='_blank'>".$game_informations_row['game_name']." <sub>(Neuer Tab)</sub></a>", $game_informations_row['game_more_info']);
										echo "<div class='col-md-7'><i class='fa fa-steam fa-fw'></i> ".$game_more_info."</a></div>";
									echo "</div>";
								}
							echo "</div>";
							echo "<div class='modal-footer'>";
								echo "<button type='button' class='btn btn-inverse' data-dismiss='modal'>Schließen</button>";
							echo "</div>";
						echo "</div>";
					echo "</div>";
				echo "</div>";

				echo "<div class='modal fade' id='buy".$row['id']."' tabindex='-1' role='dialog' aria-labelledby='myModalLabel'>";
					echo "<div class='modal-dialog' role='document'>";
						echo "<div class='modal-content'>";
							echo "<form method='post'>";
								echo "<div class='modal-header'>";
									echo "<button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button>";
									echo "<h4 class='modal-title' id='myModalLabel'>Bestellung von &quot;".$row['article_name']."&quot;</h4>";
								echo "</div>";
								echo "<div class='modal-body'>";
								if($game_informations_row['game_usk'] > 16){
									echo "<div class='form-group'>";
										echo "<label>Geburtsdatum</label>";
										echo "<div class='row'>";
											echo "<div class='col-sm-4'>";
												echo "<select name='birth_d' class='form-control' required>";
													echo "<option selected disabled>Tag</option>";
													$date_d = 1;
													while ($date_d <= 31) {
														echo "<option value='".$date_d."'>".$date_d."</option>";
														$date_d++;
													}
												echo "</select>";
											echo "</div>";
											echo "<div class='col-sm-4'>";
												echo "<select name='birth_m' class='form-control' required>";
													echo "<option selected disabled>Monat</option>";
													$date_m = array("Januar","Februar","März","April","Mai","Juni","Juli","August","September","Oktober","November","Dezember");
													reset($date_m);
													while (list(, $value) = each($date_m)) {
														echo "<option value='".$value."'>".$value."</option>";
													}
												echo "</select>";
											echo "</div>";
											echo "<div class='col-sm-4'>";
												echo "<select name='birth_y' class='form-control' required>";
													echo "<option selected disabled>Jahr</option>";
													$date_Y = (date("Y") - 80);
													while ($date_Y <= (date("Y") - 18)) {
														echo "<option value='".$date_Y."'>".$date_Y."</option>";
														$date_Y++;
													}
												echo "</select>";
											echo "</div>";
										echo "</div>";
									echo "</div>";
									echo "<div class='form-group'>";
										echo "<label>Richtigkeit deiner Daten</label>";
										echo "<div class='checkbox'>";
											echo "<label>";
												echo "<input type='checkbox' data-switch-no-init required> Hiermit bestätige ich, das ich 18 Jahre oder älter bin. Falls ich falsche Angaben getätigt habe, kann ChaosDidi.de mir eine Rechnung in Höhe von ".$game_informations_row['game_price']." € zukommen lassen.";
											echo "</label>";
										echo "</div>";
									echo "</div>";
									echo "<hr/>";
								}
									echo "<div class='row'>";
										echo "<div class='col-xs-5 text-right'><strong>Spielname:</strong></div>";
										echo "<div class='col-xs-7'>".$game_informations_row['game_name']."</div>";
									echo "</div>";
									echo "<div class='row'>";
										echo "<div class='col-xs-5 text-right'><strong>Steam Preis:</strong></div>";
										echo "<div class='col-xs-7'>".$game_informations_row['game_price']." €</div>";
									echo "</div>";
									echo "<div class='row'>";
										echo "<div class='col-xs-5 text-right'><strong>USK:</strong></div>";
										echo "<div class='col-xs-7'>".$game_informations_row['game_usk']."</div>";
									echo "</div>";
									echo "<div class='row'>";
										echo "<div class='col-xs-5 text-right'><strong>Betriebssystem:</strong></div>";
										echo "<div class='col-xs-7'>".$game_informations_row['bs']."</div>";
									echo "</div>";
									echo "<hr/>";
									echo "<div class='row'>";
										echo "<div class='col-xs-5 text-right'><strong>Aktueller Kontostand:</strong></div>";
										echo "<div class='col-xs-7'>".CREDITS." Pkt.</div>";
									echo "</div>";
									echo "<div class='row'>";
										echo "<div class='col-xs-5 text-right'><strong>Preis für <u>".$game_informations_row['game_name']."</u>:</strong></div>";
										echo "<div class='col-xs-7'>".$row['article_price']." Pkt.</div>";
									echo "</div>";
									echo "<div class='row'>";
										echo "<div class='col-xs-5 text-right'><strong>Neuer Kontostand:</strong></div>";
										echo "<div class='col-xs-7'>".(CREDITS - $row['article_price'])." Pkt.</div>";
									echo "</div>";
								echo "</div>";
								echo "<div class='modal-footer'>";
									echo "<button type='button' class='btn btn-inverse' data-dismiss='modal'>Schließen</button>";
									echo "<button type='submit' name='buyKey_".$row['id']."_sub' class='btn btn-primary'>Kaufen</button>";
								echo "</div>";
							echo "</form>";
						echo "</div>";
					echo "</div>";
				echo "</div>";
			}
		?>
		</tbody>
	</table>
</div>