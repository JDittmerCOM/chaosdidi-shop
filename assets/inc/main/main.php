<?
	$sql_domains = mysqli_num_rows(mysqli_query($db_shop, "SELECT * FROM domains"));
	$sql_steamkeys = mysqli_num_rows(mysqli_query($db_shop, "SELECT * FROM steam_keys"));
?>

<div class="box">
	<h2>Übersicht</h2>
	<div class="row">
		<div class="col-xs-1 text-right"><? echo $sql_domains; ?></div>
		<div class="col-xs-11"> <a href="Buy/Domains">Domains</a> im Shop</div>
	</div>
	<div class="row">
		<div class="col-xs-1 text-right"><? echo $sql_steamkeys; ?></div>
		<div class="col-xs-11"> <a href="Buy/SteamKeys">Steam-Keys</a> im Shop</div>
	</div>
</div>