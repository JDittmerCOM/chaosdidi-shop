<? header("Location: 404"); ?>
<div class="box">
	<h2>Suche nach <u><? if(strlen($_POST['q']) == 0){echo "<i>rein gar nichts</i>";}else{echo $_POST['q'];} ?></u></h2>
</div>

<div class="box">
	<h2>Mitglieder</h2>
	<?
		if(isset($_POST['sub_search'])){
			$query = mysqli_real_escape_string($db, $_POST['q']);

			$query = explode(" ", $query);
			$count_null = -1;
			$count_suche = count($query)-1;
			$query_erg = "";
			while ($count_null < $count_suche) {$count_null++; $query_erg .= "+".$query[$count_null]." ";}

			$query_sql = mysqli_query($db, "SELECT * FROM users WHERE username LIKE '%".mysqli_real_escape_string($db, $_POST['q'])."%'");
			if(mysqli_num_rows($query_sql) == 0 || strlen($_POST['q']) == 0){
				echo bad("Mit deinem <strong>Suchergebnis</strong> konnten wir <strong><u>keine Benutzer</u></strong> finden.");
			}else{
				while($row = mysqli_fetch_assoc($query_sql)){
					echo "<a href='MyProfile/".$row['id']."' class='btn btn-default btn-block btn-sm'>".str_ireplace($_POST['q'], "<b>".$_POST['q']."</b>", $row['username'])."</a>";
				}
			}
		}else{
			echo bad("Mit deinem <strong>Suchergebnis</strong> konnten wir <strong><u>keine Benutzer</u></strong> finden.");
		}
	?>
</div>

<div class="box">
	<h2>Videos</h2>
	<?
		if(isset($_POST['sub_search'])){
			$query = mysqli_real_escape_string($db, $_POST['q']);

			$query = explode(" ", $query);
			$count_null = -1;
			$count_suche = count($query)-1;
			$query_erg = "";
			while ($count_null < $count_suche) {$count_null++; $query_erg .= "+".$query[$count_null]." ";}

			$query_sql = mysqli_query($db, "SELECT * FROM videos WHERE MATCH(title) AGAINST('".$query_erg."*' IN BOOLEAN MODE)");
			if(mysqli_num_rows($query_sql) == 0 || strlen($_POST['q']) == 0){
				echo bad("Mit deinem <strong>Suchergebnis</strong> konnten wir <strong><u>keine Videos</u></strong> finden.");
			}else{
				while($row = mysqli_fetch_assoc($query_sql)){
					echo "<a href='Post/".$row['id']."' class='btn btn-default btn-block btn-sm'>".str_ireplace($_POST['q'], "<b>".$_POST['q']."</b>", $row['title'])."</a>";
				}
			}
		}else{
			echo bad("Mit deinem <strong>Suchergebnis</strong> konnten wir <strong><u>keine Videos</u></strong> finden.");
		}
	?>
</div>