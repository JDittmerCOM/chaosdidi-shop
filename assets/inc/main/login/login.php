<? if(ONLINE == '1'){header("Location:".SERVER_NAME);} ?>

<div class="box registration-form">
    <h2 id="alert">Einloggen</h2>
    <?
        if($_GET['success'] == '1'){
            echo good("Du hast dich <strong><u>erfolgreich</u></strong> abgemeldet.");
        }

        if(isset($_POST['sub_login'])){
            $username_email = mysqli_real_escape_string($db, $_POST['username_email']);
            $password = mysqli_real_escape_string($db, crypt($_POST['password'], CRYPT_PW));
            $http_lang = explode(",", $_SERVER['HTTP_ACCEPT_LANGUAGE']);

            if(!preg_match("/\b[\w\.-]+@[\w\.-]+\.\w{2,6}\b/", $username_email)){
                $sql = mysqli_query($db, "SELECT * FROM users WHERE username = '".$username_email."'");
                $row = mysqli_fetch_assoc($sql);

                //CHECK USERNAME
                if(strlen($username_email) < 4){
                    echo bad("Dein <strong>Benutzername</strong> ist <strong><u>zu kurz</u></strong>.<br>Bitte <strong><u>mindestens 4 Zeichen</u></strong> verwenden.");
                }else{
                    $check__username = true;
                }

                if($username_email != $row['username']){
                    echo bad("Dein <strong>Benutzername</strong> ist <strong><u>nicht Registriert</u></strong>.");
                }else{
                    $check__username2 = true;
                }

                if(!preg_match("/^[a-zA-Z0-9_.-]*$/", $username_email)){
                    echo bad("Dein <strong>Benutzername</strong> ist <strong><u>ungültig</u></strong>.<br>Bitte nur <strong>a-z A-Z 0-9 _ -</strong> verwenden.");
                }else{
                    $check__username3 = true;
                }


                //CHECK PASSWORD
                if($password != $row['password']){
                    echo bad("Dein <strong>Passwort</strong> ist <strong><u>falsch</u></strong>.");

                    if(mysqli_num_rows(mysqli_query($db, "SELECT * FROM login_attempts WHERE login_username = '".$username_email."'")) < 3){
                        mysqli_query($db, "INSERT INTO login_attempts (`date`, ip, login_userID, login_username) VALUES ('".time()."', '".$_SERVER['REMOTE_ADDR']."', '".$row['id']."', '".$username_email."')");
                        if(mysql_error()){exit(mysql_error());}
                    }else{
                        $activate_code = rand(1000000000,9999999999);
                        mysqli_query($db, "UPDATE users SET activate_code = 'UB".$activate_code."', status = '0' WHERE username = '".$username_email."'");
                        if(mysql_error()){exit(mysql_error());}
                        mysqli_query($db, "INSERT INTO users_banned (`date`, userID, message, by_admin) VALUES ('".time()."', '".$row['id']."', 'Dein Account wurde aufgrund zu vieler Log-in-Versuche gesperrt.<br>Bitte klicke auf den Link in der eben erhaltenden eMail von uns oder melde dich bei einem Administrator.', 'System')");
                        if(mysql_error()){exit(mysql_error());}

                        echo "<div class='alert alert-danger'>";
                            echo "<strong><u>Dein Benutzerkonto ist gesperrt.</u></strong><br>";
                            echo "<div class='row'>";
                                echo "<div class='col-md-2 text-right'><strong>Datum:</strong></div>";
                                echo "<div class='col-md-10'>".date("d.m.Y - H:i:s", time())." Uhr</div>";
                            echo "</div>";
                            echo "<div class='row'>";
                                echo "<div class='col-md-2 text-right'><strong>Grund:</strong></div>";
                                echo "<div class='col-md-10'>Dein Account wurde aufgrund zu vieler Log-in-Versuche gesperrt.<br>Bitte klicke auf den Link in der eben erhaltenden eMail von uns oder melde dich bei einem Administrator.</div>";
                            echo "</div>";
                            echo "<div class='row'>";
                                echo "<div class='col-md-2 text-right'><strong>Von:</strong></div>";
                                echo "<div class='col-md-10'>System</div>";
                            echo "</div>";
                        echo "</div>";

                        $mail_to = "ban@chaosdidi.de";
                        $mail_subject = "[User Banned] ".$row['username']." wurde gesperrt";
                        $mail_message = '
                        <html>
                            <head>
                                <title>[User Banned] '.$row['username'].' wurde gesperrt</title>
                                <link href="http://chaosdidi.de/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
                            </head>
                            <body>
                                <br>
                                <div class="container">
                                    Das Mitglied <strong>'.$row['username'].'</strong> wurde heute am <strong>'.date("d.m.Y").' um '.date("H:i:s").'</strong> von <strong>System</strong> gesperrt.<br>
                                    <hr/>
                                    <div class="row">
                                        <div class="col-md-3 text-right"><strong>Grund für diese Sperre:</strong></div>
                                        <div class="col-md-9">
                                            Dein Account wurde aufgrund zu vieler Log-in-Versuche gesperrt.<br>
                                            Bitte klicke auf den Link in der eben erhaltenden eMail von uns oder melde dich bei einem Administrator.
                                        </div>
                                    </div>
                                </div>
                            </body>
                        </html>
                        ';

                        // Always set content-type when sending HTML email
                        $mail_headers = "MIME-Version: 1.0" . "\n";
                        $mail_headers .= "Content-type:text/html;charset=UTF-8" . "\n";
                        $mail_headers .= 'From: ChaosDidi <ban@chaosdidi.de>' . "\n";

                        mail($mail_to,$mail_subject,$mail_message,$mail_headers);

                        $to = $row['email'];
                        $subject = "Dein Account wurde auf ChaosDidi.de gesperrt!";
                        $message = '
                        <html>
                            <head>
                                <title>Dein Account wurde auf ChaosDidi.de gesperrt!</title>
                                <link href="http://chaosdidi.de/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
                            </head>
                            <body>
                                <br>
                                <div class="container">
                                    <img src="http://chaosdidi.de/assets/images/logo.png" class="img-responsive">
                                    <br><br>
                                    Aufgrund zu vieler Log-in-Versuche, wurde dein Account aus Sicherheitsgründen auf ChaosDidi.de gesperrt.
                                    <hr/>
                                    Falls du es warst, dann klicke auf den unten stehenden Link um deinen Account wieder freizuschalten.<br>
                                    <a href="http://chaosdidi.de/LoginActivate/UB'.$activate_code.'">Dein Link, zum freischalten deines Accounts</a><br>
                                    Falls du es <strong><u>nicht</u></strong> warst, dann <a href="mailto:admin@chaosdidi.de">melde dich</a> umgehend bei uns, damit wir uns darum schnellstmöglich kümmern können.<br><br>

                                    Viel Glück wünscht dir,<br>
                                    Das ChaosDidi Team
                                    <br><br>
                                </div>
                            </body>
                        </html>
                        ';

                        // Always set content-type when sending HTML email
                        $headers = "MIME-Version: 1.0" . "\n";
                        $headers .= "Content-type:text/html;charset=UTF-8" . "\n";
                        $headers .= 'From: ChaosDidi <no-reply@chaosdidi.de>' . "\n";

                        mail($to,$subject,$message,$headers);
                    }
                }else{
                    $check_password = true;
                }


                //CHECK STATUS
                if($row['status'] == '0'){
                    $users_banned_sql = mysqli_query($db, "SELECT * FROM users_banned WHERE userID = '".$row['id']."'");
                    $users_banned_row = mysqli_fetch_assoc($users_banned_sql);
                    echo "<div class='alert alert-danger'>";
                        echo "<strong><u>Dein Benutzerkonto ist gesperrt.</u></strong><br>";
                        echo "<div class='row'>";
                            echo "<div class='col-md-2 text-right'><strong>Datum:</strong></div>";
                            echo "<div class='col-md-10'>".date("d.m.Y - H:i:s", $users_banned_row['date'])." Uhr</div>";
                        echo "</div>";
                        echo "<div class='row'>";
                            echo "<div class='col-md-2 text-right'><strong>Grund:</strong></div>";
                            echo "<div class='col-md-10'>".$users_banned_row['message']."</div>";
                        echo "</div>";
                        echo "<div class='row'>";
                            echo "<div class='col-md-2 text-right'><strong>Von:</strong></div>";
                            echo "<div class='col-md-10'>".$users_banned_row['by_admin']."</div>";
                        echo "</div>";
                        echo "<u>Missverständnis?</u> <a href='mailto:support@chaosdidi.de?subject=Mein Benutzerkonto wurde gesperrt&body=Nutzer-ID: ".$row['id']."%0AGrund: (Hier bitte deinen Grund zur Entsperrung angeben)'><strong>Hier</strong></a> melden!";
                    echo "</div>";
                }

                if($row['status'] == '1'){
                    echo bad("Dein <strong>Benutzerkonto</strong> ist noch <u>nicht</u> freigeschaltet.");
                }

                if($row['status'] == '2'){
                    $check__status = true;
                }


                if(
                    $check__username == true && $check__username2 == true && $check__username3 == true
                    && $check_password == true
                    && $check__status == true){
                    $_SESSION['login'] = array("user_id" => $row['id']);
                    mysqli_query($db, "UPDATE users SET http_lang = '".$http_lang[0]."', activity_email = '0', online = '1' WHERE id = '".$row['id']."'");
                    if(mysql_error()){exit(mysql_error());}
                    mysqli_query($db, "DELETE FROM login_attempts WHERE login_userID = '".$row['id']."'");
                    if(mysql_error()){exit(mysql_error());}
                    header("Location:".SERVER_NAME);
                }
            }elseif(preg_match("/\b[\w\.-]+@[\w\.-]+\.\w{2,6}\b/", $username_email)){
                $sql = mysqli_query($db, "SELECT * FROM users WHERE email = '".$username_email."'");
                $row = mysqli_fetch_assoc($sql);

                //CHECK EMAIL
                if(strlen($username_email) < 8){
                    echo bad("Deine <strong>eMail Adresse</strong> ist <strong><u>zu kurz</u></strong>.<br>Bitte <strong><u>mindestens 8 Zeichen</u></strong> verwenden.");
                }else{
                    $check__email = true;
                }

                if($username_email != $row['email']){
                    echo bad("Deine <strong>eMail Adresse</strong> ist <strong><u>nicht Registriert</u></strong>.");
                }else{
                    $check__email2 = true;
                }

                if(!preg_match("/\b[\w\.-]+@[\w\.-]+\.\w{2,6}\b/", $username_email)){
                    echo bad("Deine <strong>eMail Adresse</strong> ist <strong><u>ungültig</u></strong>.<br>Bitte nur <strong>a-z A-Z 0-9 _ . - @</strong> verwenden.");
                }else{
                    $check__email3 = true;
                }


                //CHECK PASSWORD
                if($password != $row['password']){
                    echo bad("Dein <strong>Passwort</strong> ist <strong><u>falsch</u></strong>.");

                    if(mysqli_num_rows(mysqli_query($db, "SELECT * FROM login_attempts WHERE login_username = '".$username_email."'")) < 3){
                        mysqli_query($db, "INSERT INTO login_attempts (`date`, ip, login_userID, login_username) VALUES ('".time()."', '".$_SERVER['REMOTE_ADDR']."', '".$row['id']."', '".$username_email."')");
                        if(mysql_error()){exit(mysql_error());}
                    }else{
                        $activate_code = rand(1000000000,9999999999);
                        mysqli_query($db, "UPDATE users SET activate_code = 'UB".$activate_code."', status = '0' WHERE username = '".$username_email."'");
                        if(mysql_error()){exit(mysql_error());}
                        mysqli_query($db, "INSERT INTO users_banned (`date`, userID, message, by_admin) VALUES ('".time()."', '".$row['id']."', 'Dein Account wurde aufgrund zu vieler Log-in-Versuche gesperrt.<br>Bitte klicke auf den Link in der eben erhaltenden eMail von uns oder melde dich bei einem Administrator.', 'System')");
                        if(mysql_error()){exit(mysql_error());}

                        echo "<div class='alert alert-danger'>";
                            echo "<strong><u>Dein Benutzerkonto ist gesperrt.</u></strong><br>";
                            echo "<div class='row'>";
                                echo "<div class='col-md-2 text-right'><strong>Datum:</strong></div>";
                                echo "<div class='col-md-10'>".date("d.m.Y - H:i:s", time())." Uhr</div>";
                            echo "</div>";
                            echo "<div class='row'>";
                                echo "<div class='col-md-2 text-right'><strong>Grund:</strong></div>";
                                echo "<div class='col-md-10'>Dein Account wurde aufgrund zu vieler Log-in-Versuche gesperrt.<br>Bitte klicke auf den Link in der eben erhaltenden eMail von uns oder melde dich bei einem Administrator.</div>";
                            echo "</div>";
                            echo "<div class='row'>";
                                echo "<div class='col-md-2 text-right'><strong>Von:</strong></div>";
                                echo "<div class='col-md-10'>System</div>";
                            echo "</div>";
                        echo "</div>";

                        $mail_to = "ban@chaosdidi.de";
                        $mail_subject = "[User Banned] ".$row['username']." wurde gesperrt";
                        $mail_message = '
                        <html>
                            <head>
                                <title>[User Banned] '.$row['username'].' wurde gesperrt</title>
                                <link href="http://chaosdidi.de/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
                            </head>
                            <body>
                                <br>
                                <div class="container">
                                    Das Mitglied <strong>'.$row['username'].'</strong> wurde heute am <strong>'.date("d.m.Y").' um '.date("H:i:s").'</strong> von <strong>System</strong> gesperrt.<br>
                                    <hr/>
                                    <div class="row">
                                        <div class="col-md-3 text-right"><strong>Grund für diese Sperre:</strong></div>
                                        <div class="col-md-9">
                                            Dein Account wurde aufgrund zu vieler Log-in-Versuche gesperrt.<br>
                                            Bitte klicke auf den Link in der eben erhaltenden eMail von uns oder melde dich bei einem Administrator.
                                        </div>
                                    </div>
                                </div>
                            </body>
                        </html>
                        ';

                        // Always set content-type when sending HTML email
                        $mail_headers = "MIME-Version: 1.0" . "\n";
                        $mail_headers .= "Content-type:text/html;charset=UTF-8" . "\n";
                        $mail_headers .= 'From: ChaosDidi <ban@chaosdidi.de>' . "\n";

                        mail($mail_to,$mail_subject,$mail_message,$mail_headers);

                        $to = $row['email'];
                        $subject = "Dein Account wurde auf ChaosDidi.de gesperrt!";
                        $message = '
                        <html>
                            <head>
                                <title>Dein Account wurde auf ChaosDidi.de gesperrt!</title>
                                <link href="http://chaosdidi.de/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
                            </head>
                            <body>
                                <br>
                                <div class="container">
                                    <img src="http://chaosdidi.de/assets/images/logo.png" class="img-responsive">
                                    <br><br>
                                    Aufgrund zu vieler Log-in-Versuche, wurde dein Account aus Sicherheitsgründen auf ChaosDidi.de gesperrt.
                                    <hr/>
                                    Falls du es warst, dann klicke auf den unten stehenden Link um deinen Account wieder freizuschalten.<br>
                                    <a href="http://chaosdidi.de/LoginActivate/UB'.$activate_code.'">Dein Link, zum freischalten deines Accounts</a><br>
                                    Falls du es <strong><u>nicht</u></strong> warst, dann <a href="mailto:admin@chaosdidi.de">melde dich</a> umgehend bei uns, damit wir uns darum schnellstmöglich kümmern können.<br><br>

                                    Viel Glück wünscht dir,<br>
                                    Das ChaosDidi Team
                                    <br><br>
                                </div>
                            </body>
                        </html>
                        ';

                        // Always set content-type when sending HTML email
                        $headers = "MIME-Version: 1.0" . "\n";
                        $headers .= "Content-type:text/html;charset=UTF-8" . "\n";
                        $headers .= 'From: ChaosDidi <no-reply@chaosdidi.de>' . "\n";

                        mail($to,$subject,$message,$headers);
                    }
                }else{
                    $check_password = true;
                }


                //CHECK STATUS
                if($row['status'] == '0'){
                    $users_banned_sql = mysqli_query($db, "SELECT * FROM users_banned WHERE userID = '".$row['id']."'");
                    $users_banned_row = mysqli_fetch_assoc($users_banned_sql);
                    echo "<div class='alert alert-danger'>";
                        echo "<strong><u>Dein Benutzerkonto ist gesperrt.</u></strong><br>";
                        echo "<div class='row'>";
                            echo "<div class='col-md-2 text-right'><strong>Datum:</strong></div>";
                            echo "<div class='col-md-10'>".date("d.m.Y - H:i:s", $users_banned_row['date'])." Uhr</div>";
                        echo "</div>";
                        echo "<div class='row'>";
                            echo "<div class='col-md-2 text-right'><strong>Grund:</strong></div>";
                            echo "<div class='col-md-10'>".$users_banned_row['message']."</div>";
                        echo "</div>";
                        echo "<div class='row'>";
                            echo "<div class='col-md-2 text-right'><strong>Von:</strong></div>";
                            echo "<div class='col-md-10'>".$users_banned_row['by_admin']."</div>";
                        echo "</div>";
                        echo "<u>Missverständnis?</u> <a href='mailto:support@chaosdidi.de?subject=Mein Benutzerkonto wurde gesperrt&body=Nutzer-ID: ".$row['id']."%0AGrund: (Hier bitte deinen Grund zur Entsperrung angeben)'><strong>Hier</strong></a> melden!";
                    echo "</div>";
                }

                if($row['status'] == '1'){
                    echo bad("Dein <strong>Benutzerkonto</strong> ist noch <u>nicht</u> freigeschaltet.");
                }

                if($row['status'] == '2'){
                    $check__status = true;
                }


                if(
                    $check__email == true && $check__email2 == true && $check__email3 == true
                    && $check_password == true
                    && $check__status == true){
                    $_SESSION['login'] = array("user_id" => $row['id']);
                    mysqli_query($db, "UPDATE users SET http_lang = '".$http_lang[0]."', activity_email = '0', online = '1' WHERE id = '".$row['id']."'");
                    if(mysql_error()){exit(mysql_error());}
                    mysqli_query($db, "DELETE FROM login_attempts WHERE login_userID = '".$row['id']."'");
                    if(mysql_error()){exit(mysql_error());}
                    header("Location:".SERVER_NAME);
                }
            }
        }
    ?>
    <form method="post">
        <div class="form-group">
            <label for="login_username">Benutzername oder eMail Adresse</label>
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-user fa-fw"></i></div>
                <input type="text" name="username_email" class="form-control" id="login_username" placeholder="Benutzername oder eMail Adresse" <? echo "value='".$username_email."'"; ?> maxlength="100" required>
            </div>
        </div>
        <div class="form-group">
            <label for="login_pass">Passwort</label>
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-lock fa-fw"></i></div>
                <input type="password" name="password" class="form-control" id="login_pass" placeholder="Passwort" maxlength="35" required>
            </div>
        </div>
        <button type="submit" name="sub_login" class="btn btn-primary login-btn">Einloggen</button>
    </form>
</div>

<div class="box registration-form">
    <h2>Passwort vergessen?</h2>
    <p>Wenn du dein Passwort vergessen haben solltest, dann kannst du mithilfe dieses Formulars, dein Passwort zurücksetzen. Das neue Passwort wird an deine Email geschickt.</p>
    <?
        if(isset($_POST['sub_forgotten_password'])){
            $email = mysqli_real_escape_string($db, $_POST['email']);

            $sql = mysqli_query($db, "SELECT * FROM users WHERE email = '".$email."'");
            $row = mysqli_fetch_assoc($sql);

            //CHECK ACTIVATE CODE and STATUS
            if(strlen($row['activate_code']) === 0 && $row['status'] === 0){
                echo bad("<strong>Du wurdest</strong> durch einen Administrator oder Moderator <strong>gebannt</strong>.<br>Dir wurde <strong>kein neues Passwort</strong> generiert.");
            }else{
                $check__activate_code = true;
            }

            //CHECK EMAIL
            if(strlen($email) < 8){
                echo bad("Deine <strong>eMail Adresse</strong> ist <strong><u>zu kurz</u></strong>.<br>Bitte <strong><u>mindestens 8 Zeichen</u></strong> verwenden.");
            }else{
                $check__email = true;
            }

            if(!preg_match("/\b[\w\.-]+@[\w\.-]+\.\w{2,6}\b/", $email)){
                echo bad("Deine <strong>eMail Adresse</strong> ist <strong><u>ungültig</u></strong>.<br>Bitte nur <strong>a-z A-Z 0-9 _ . - @</strong> verwenden.");
            }else{
                $check__email2 = true;
            }

            if($email != $row['email']){
                echo bad("Deine <strong>eMail Adresse</strong> ist <strong><u>falsch</u></strong>.");
            }else{
                $check__email3 = true;
            }


            if($check__activate_code == true
                && $check__email == true && $check__email2 == true && $check__email3 == true){
                $random_password_ori = rand(10000000000000000,99999999999999999);
                $random_password_hash = crypt($random_password_ori, CRYPT_PW);
                mysqli_query($db, "UPDATE users SET password = '".$random_password_hash."' WHERE id = '".$row['id']."'");
                if(mysql_error()){exit(mysql_error());}
                mysqli_query($db, "DELETE FROM users_banned WHERE userID = '".$row['id']."'");
                if(mysql_error()){exit(mysql_error());}

                echo good("Ein neues Passwort wurde an deine eMail Adresse gesendet.");

                $to = $row['email'];
                $subject = "Dein neues Passwort für ChaosDidi.de";
                $message = '
                <html>
                    <head>
                        <title>Dein neues Passwort für ChaosDidi.de</title>
                        <link href="http://chaosdidi.de/assets/css/bootstrap.min.css" rel="stylesheet" type="text/css">
                    </head>
                    <body>
                        <br>
                        <div class="container">
                            <img src="http://chaosdidi.de/assets/images/logo.png" class="img-responsive">
                            <br><br>
                            Du hast auf ChaosDidi.de ein neues Passwort angefordert.
                            <br><br>
                            Damit du dich wieder Einloggen kannst, findest du unten deine Logindaten mit deinem neuen Passwort.
                            <hr/>
                            <div class="row">
                                <div class="col-md-3 text-right"><strong><u>Deine Logindaten:</u></strong></div>
                                <div class="col-md-9"></div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 text-right"><strong>Benutzername:</strong></div>
                                <div class="col-md-9">'.$row['username'].'</div>
                            </div>
                            <div class="row">
                                <div class="col-md-3 text-right"><strong>Passwort:</strong></div>
                                <div class="col-md-9">'.$random_password_ori.'</div>
                            </div>
                            <hr/>
                            Viel Spaß wünscht dir,<br>
                            Das ChaosDidi Team<br><br>
                        </div>
                    </body>
                </html>
                ';

                // Always set content-type when sending HTML email
                $headers = "MIME-Version: 1.0" . "\n";
                $headers .= "Content-type:text/html;charset=UTF-8" . "\n";
                $headers .= 'From: ChaosDidi <no-reply@chaosdidi.de>' . "\n";

                mail($to,$subject,$message,$headers);
            }
        }
    ?>
    <form method="post">
        <div class="form-group">
            <label for="forg_email">eMail Adresse</label>
            <div class="input-group">
                <div class="input-group-addon"><i class="fa fa-envelope fa-fw"></i></div>
                <input type="email" name="email" class="form-control" id="forg_email" placeholder="eMail Adresse" maxlength="100" required>
            </div>
        </div>
        <button type="submit" name="sub_forgotten_password" class="btn btn-primary">Passwort zurücksetzen</button>
    </form>
</div>