<!-- SIDEBAR BOX - START -->
<div class="box sidebar-box widget-wrapper">
	<h3>Navigation</h3>
    <a href="Admin"><b><u>Admin-Startseite</u></b></a>
    <br>
    <div class="btn-group-vertical" style="width:100%">
        <div class="btn-group" style="width:100%">
            <button type="button" class="btn btn-primary dropdown-toggle btn-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-bar-chart fa-fw"></i> Analytics <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="Admin/Analytics/SteamKeys"><i class="fa fa-bar-chart fa-fw"></i> Verkaufte Steam-Keys</a></li>
                <li><a href="Admin/Analytics/Domains"><i class="fa fa-bar-chart fa-fw"></i> Verkaufte Domains</a></li>
            </ul>
        </div>
        <div class="btn-group" style="width:100%">
            <button type="button" class="btn btn-primary dropdown-toggle btn-block" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="fa fa-steam fa-fw"></i> Steam-Keys <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="Admin/SteamKeys/Create"><i class="fa fa-plus-square fa-fw"></i> Steam-Key hinzufügen</a></li>
            </ul>
        </div>
    </div>
</div>
<!-- SIDEBAR BOX - END -->