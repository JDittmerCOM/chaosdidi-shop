<? define("SERVER_NAME", "//".$_SERVER['SERVER_NAME']."/");

    function good($message){
        $alert = "<div class='alert alert-success'>".$message."</div>";
        return $alert;
    }
    function bad($message){
        $alert = "<div class='alert alert-danger'>".$message."</div>";
        return $alert;
    }
    function warning($message){
        $alert = "<div class='alert alert-warning'>".$message."</div>";
        return $alert;
    }
    function info($message){
        $alert = "<div class='alert alert-info'>".$message."</div>";
        return $alert;
    }


    tld($_POST['domain_name'], $_POST['domain_tld']);
    function tld($domain, $tld){
        $result = dns_get_record($domain.$tld.".");
        if(!empty($result)){ $tld_status = "BELEGT"; }else{ $tld_status = "FREI"; }

        if($tld_status == "FREI"){
            echo good("<strong>Diese Domain ist Verfügbar!</strong>");
        }else{
            echo bad("<strong>Diese Domain ist bereits Belegt!</strong>");
        }
    } # FUNKTION END
?>