/*------------------------------
 * Copyright 2015 Pixelized
 * http://www.pixelized.cz
 *
 * Progaming theme v1.0
------------------------------*/

$(document).ready(function() {	
	
	//NAVBAR
	$('.navbar .nav > li.dropdown').mouseenter(function() {
		$(this).addClass('open');
	});
	
	$('.navbar .nav > li.dropdown').mouseleave(function() {
		$(this).removeClass('open');
	});
	
	$.vegas('slideshow', {
		delay:5000,
		backgrounds:[
			{ src:'assets/images/hp_bg/life_is_strange.jpg', fade:1500 },
			{ src:'assets/images/hp_bg/evoland.jpg', fade:1500 },
			{ src:'assets/images/hp_bg/the_evil_within.jpg', fade:1500 },
			{ src:'assets/images/hp_bg/castle_storm.jpg', fade:1500 },
			{ src:'assets/images/hp_bg/mirrors_edge.jpg', fade:1500 },
			{ src:'assets/images/hp_bg/singularity.jpg', fade:1500 },
			{ src:'assets/images/hp_bg/mass_effect.jpg', fade:1500 },
		]
	})('overlay', {src:'assets/images/overlay.png'});
	
	//SCROLLING
	$("a.scroll[href^='#']").on('click', function(e) {
		e.preventDefault();
		var hash = this.hash;
		$('html, body').animate({ scrollTop: $(this.hash).offset().top - 110}, 1000, function(){window.location.hash = hash;});
	});
	
	//BLOG POST - CAROUSEL
	$(".gallery-post .owl-carousel").owlCarousel({
		navigation : true,
		singleItem : true,
		pagination : false,
		autoPlay: 5000,
		slideSpeed : 500,
		navigationText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
	});
	
	//OWL CAROUSEL
	$("#jumbotron-slider").owlCarousel({
		pagination : false,
		itemsDesktop :[1200,3],
		itemsDesktopSmall :[991,2],
		items : 3,
		navigation : true,
		navigationText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
	});
	
	$('#open-search').on('click', function() {
		$(this).toggleClass('show2 hidden');
		$('#close-search').toggleClass('show2 hidden');
		$("#navbar-search-form").toggleClass('show2 hidden animated fadeInDown');
		return false;
	});
	$('#close-search').on('click', function() {
		$(this).toggleClass('show2 hidden');
		$('#open-search').toggleClass('show2 hidden');;
		$("#navbar-search-form").toggleClass('fadeInDown fadeOutUp');
		setTimeout(function(){
			$("#navbar-search-form").toggleClass('show2 hidden animated fadeOutUp');
		}, 500);
		return false;
	});
	
	$('.gallery-page').each(function() { // the containers for all your galleries
		$(this).magnificPopup({
			delegate: 'div a',
			type: 'image',
			tLoading: 'Lädt Bild #%curr%...',
			mainClass: 'mfp-img-mobile',
			gallery: {
				enabled: true,
				navigateByImgClick: true,
				preload: [0,1] // Will preload 0 - before current, and 1 after the current image
			},
			image: {
				tError: '<a href="%url%">Das Bild #%curr%</a> konnte nicht geladen werden.',
				titleSrc: function(item) {
					return item.el.attr('title');
				}
			}
		});
	}); 
	
	
	//FORM TOGGLE
	$('#reset-password-toggle').click(function() {
		$('#reset-password').slideToggle(500);
	});
});

// FADEOUT OF A SUCCESS REPORT [start]
setTimeout(function(){
    $('#success_report').fadeOut("slow");
}, 2000);
// FADEOUT OF A SUCCESS REPORT [end]


//WORDFILTER ADD
function wordfilter_add(){
	var word = $('#word').val();
	$.ajax({
		type: "POST",
		url: "assets/js/wordfilter/word_add.php",
		data: {
			word: word
		},
		success: function(){
			$("#addWord").append("\
				<tr>\
	            	<td>"+word+"</td>\
	            	<td>*****</td>\
	            </tr>\
            ");

			$('#word').val("");
		}
	});
}


// ADD PLAYLIST IN DATABASE [start]
function addPlaylist(){
    var namePlaylist = $('#namePlaylist').val();
    $.ajax({
        type: "POST",
        url: "assets/js/addPlaylist.php",
        data: "name="+namePlaylist,
        success: function(html){
            $("#addPlaylist").html("<label><input type='radio' name='playlists' value='"+namePlaylist+"'> "+namePlaylist+"</label><br>");
        }
    });

    $('#namePlaylist').focus();
    $('#namePlaylist').val("");
}

function UpdateHidden(){
    var checkboxes = jQuery('input[name="playlists"]');
    var values = new Array();

    checkboxes.each(function () {
        var checked = jQuery(this).is(':checked');
        var value = jQuery(this).val();

        if (checked) {
            values.push(value);
        }
    });
}

jQuery(document).ready(function () {

    UpdateHidden();

    jQuery('body').on('change', 'input[name="playlists"]', function () {
        UpdateHidden();
    });
});
// ADD PLAYLIST IN DATABASE [end]


// ADD TAGS [start]
jQuery(document).ready(function () {
    var body = jQuery('body');

    var addTag = function () {
        $('.nameTag').focus();

        var input = jQuery('.tagbox > .row > .col-md-6 > .nameTag');
        var hidden = jQuery('.tagbox > .taghidden');
        var taglist = jQuery('.tagbox > .tags > #addTag');
        if (input.val().length > 0) {
            var tagvalue = jQuery.trim(input.val());
            var tags = hidden.attr('value').split(',');

            if (jQuery.isArray(tags)) {
                if (jQuery.inArray(tagvalue, tags) >= 0) {
                    input.popover({
                        placement: "left",
                        delay: { "show": 500, "hide": 100 },
                        content: "Dieses Tag ist bereits hinzugefügt!"
                    })
                    input.popover('show');
                    setTimeout(function(){
                        input.popover('hide');
                    }, 2500);

                    input.on('hidden.bs.popover', function () {
                        input.popover('destroy');
                    });
                    return;
                }
            }else{
                return;
            }

            tags = tags.filter(function (value) {
                return (value.length > 0);
            });

            tags.push(tagvalue);
            taglist.append('<a class="tag" data-value="' + tagvalue + '"><span class="label label-default"><i class="fa fa-tag"></i> ' + tagvalue + ' <i class="fa fa-times"></i></span>&ensp;</a>');
            input.val('');

            hidden.attr('value', tags.join(','));
        }
    };

    body.on('click', '.tagbox > .tags > #addTag > .tag > span > .fa-times', function () {
        var tag = jQuery(this).closest(".tag");
        var tagvalue = tag.attr("data-value");
        var hidden = jQuery('.tagbox > .taghidden');
        var tags = hidden.attr('value').split(',');

        tags = tags.filter(function (value) {
            return (value != tagvalue);
        });


        hidden.attr('value', tags.join(','));
        tag.remove();
    });
    body.on('click', '.tagbox > .row > .col-md-6 > .addTag', function () {
        addTag();
    });
    body.on('keypress', '.tagbox > .row > .col-md-6 > .nameTag', function (event) {
        if (event.which == 13) {
            event.preventDefault();
            addTag();

        }
    });
});
// ADD TAGS [end]


// FILTERTABLE [start]
$(document).ready(function() {
    $('table').filterTable({
        callback: function(term, table) { stripeTable(table); } //call the striping after every change to the filter term
    });
});
// FILTERTABLE [end]


// DATETIMEPICKER [start]
$(function () {
    $('#posts_createapi_datetimepicker').datetimepicker({
    	format: 'DD.MM.YYYY HH:mm'
    });
});

$(document).ready(function() {
	$('button[id="save_planned_time"]').click(function(){
		var planned_time = $('#posts_createapi_datetimepicker input').val();
        $('input[name="planned_time"]').attr('value', planned_time);
    });

    $('button[id="del_planned_time"]').click(function(){
        $('input[name="planned_time"]').attr('value', '');
        $('input[name="planned_time"]').val('');
    });
});
// DATETIMEPICKER [end]


// SURVEYS ADD [start]
var SurveyCountADD = 1;
$(document).ready(function() {

    $('button[id="addSurveyFieldButtonADD"]').click(function(){
        SurveyCountADD++;
        if(SurveyCountADD > 10){
            $.amaran({
			    content:{
			    	title:'Mehr als 10 Antwortfelder kannst du nicht hinzufügen.',
			        message:'',
			        info:'Schade :(',
			        icon:'fa fa-exclamation-triangle'
			    },
			    theme:'awesome error'
			});
        }else{
            $("#addSurveyFieldADD").append("\
                <div class='form-group'>\
                	<div class='row'>\
                		<div class='col-md-2'><input type='text' name='pos"+SurveyCountADD+"' class='form-control' placeholder='"+SurveyCountADD+"' value='"+SurveyCountADD+"' maxlength='2'></div>\
                		<div class='col-md-10'><input type='text' name='answer"+SurveyCountADD+"' class='form-control' placeholder='Antwort "+SurveyCountADD+"'></div>\
                	</div>\
                </div>\
            ");
        }
    });
});
// SURVEYS ADD [end]


// SURVEYS EDIT [start]
var SurveyCountEDIT = $('input[id="SurveyCountEDIT"]').val();
$(document).ready(function() {

    $('button[id="addSurveyFieldButtonEDIT"]').click(function(){
        SurveyCountEDIT++;
        if(SurveyCountEDIT > 10){
            $.amaran({
			    content:{
			    	title:'Mehr als 10 Antwortfelder kannst du nicht hinzufügen.',
			        message:'',
			        info:'Schade :(',
			        icon:'fa fa-exclamation-triangle'
			    },
			    theme:'awesome error'
			});
        }else{
            $("#addSurveyFieldEDIT").append("\
                <div class='form-group'>\
                	<div class='row'>\
                		<div class='col-md-2'><input type='text' name='pos"+SurveyCountEDIT+"' class='form-control' placeholder='"+SurveyCountEDIT+"' value='"+SurveyCountEDIT+"' maxlength='2'></div>\
                		<div class='col-md-9'><input type='text' name='answer"+SurveyCountEDIT+"' class='form-control' placeholder='Antwort "+SurveyCountEDIT+"'></div>\
                	</div>\
                </div>\
            ");
        }
    });
});
// SURVEYS EDIT [end]


// DELETE SURVEY [start]
function delSurveyFieldEDIT(questionID, pos, answerID){
	if(confirm("Bist du dir sicher, das du dieses Antwortfeld entfernen möchtest?")){
		$.ajax({
			type: "POST",
			url: "assets/js/delete_survey_field.php",
			data: {
				questionID: questionID,
				pos: pos,
				answerID: answerID
			},
			success: function(){
				$.amaran({
				    content:{
				    	title:'Das Antwortfeld wurde entfernt und ich bin enttäuscht.',
				        message:'',
				        info:'Schade :(',
				        icon:'fa fa-check'
				    },
				    theme:'awesome ok'
				});
				$('#fieldID_'+answerID).remove();
			},
			error: function(){
				alert("Es ist ein Fehler beim Bearbeiten der Anfrage aufgetreten.\nBitte kontaktieren Sie den Administrator.\n\nFehlercode: D16A7E98");
			}
		});
	}
}
// DELETE SURVEY [end]


// DELETE MEMBER [start]
function delMember(id){
	if(confirm("Bist du dir sicher, das du dieses Mitglied löschen möchtest?")){
		$.ajax({
			type: "POST",
			url: "assets/js/delete_member.php",
			data: {
				id: id
			},
			success: function(){
				$.amaran({
				    content:{
				    	title:'Das Mitglied wurde entfernt.',
				        message:'',
				        info:'Schade :(',
				        icon:'fa fa-check'
				    },
				    theme:'awesome ok'
				});
				window.location.replace("Admin/Members/All").delay(4000);
			},
			error: function(){
				alert("Es ist ein Fehler beim Bearbeiten der Anfrage aufgetreten.\nBitte kontaktieren Sie den Administrator.");
			}
		});
	}
}
// DELETE MEMBER [end]