/*------------------------------
 * Copyright 2015 Pixelized
 * http://www.pixelized.cz
 *
 * Progaming theme v1.0
------------------------------*/

$(document).ready(function() {	
	
	//NAVBAR
	$('.navbar .nav > li.dropdown').mouseenter(function() {
		$(this).addClass('open');
	});
	
	$('.navbar .nav > li.dropdown').mouseleave(function() {
		$(this).removeClass('open');
	});
	
	$.vegas('slideshow', {
		delay:5000,
		backgrounds:[
			{ src:'assets/images/hp_bg/life_is_strange.jpg', fade:1500 },
			{ src:'assets/images/hp_bg/evoland.jpg', fade:1500 },
			{ src:'assets/images/hp_bg/the_evil_within.jpg', fade:1500 },
			{ src:'assets/images/hp_bg/castle_storm.jpg', fade:1500 },
			{ src:'assets/images/hp_bg/mirrors_edge.jpg', fade:1500 },
			{ src:'assets/images/hp_bg/singularity.jpg', fade:1500 },
			{ src:'assets/images/hp_bg/mass_effect.jpg', fade:1500 },
		]
	})('overlay', {src:'assets/images/overlay.png'});
	
	//SCROLLING
	$("a.scroll[href^='#']").on('click', function(e) {
		e.preventDefault();
		var hash = this.hash;
		$('html, body').animate({ scrollTop: $(this.hash).offset().top - 110}, 1000, function(){window.location.hash = hash;});
	});
	
	//BLOG POST - CAROUSEL
	$(".gallery-post .owl-carousel").owlCarousel({
		navigation : true,
		singleItem : true,
		pagination : false,
		autoPlay: 5000,
		slideSpeed : 500,
		navigationText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
	});
	
	//OWL CAROUSEL
	$("#jumbotron-slider").owlCarousel({
		pagination : false,
		itemsDesktop :[1200,3],
		itemsDesktopSmall :[991,2],
		items : 3,
		navigation : true,
		navigationText : ["<i class='fa fa-angle-left'></i>","<i class='fa fa-angle-right'></i>"]
	});
	
	$('#open-search').on('click', function() {
		$(this).toggleClass('show2 hidden');
		$('#close-search').toggleClass('show2 hidden');
		$("#navbar-search-form").toggleClass('show2 hidden animated fadeInDown');
		return false;
	});
	$('#close-search').on('click', function() {
		$(this).toggleClass('show2 hidden');
		$('#open-search').toggleClass('show2 hidden');;
		$("#navbar-search-form").toggleClass('fadeInDown fadeOutUp');
		setTimeout(function(){
			$("#navbar-search-form").toggleClass('show2 hidden animated fadeOutUp');
		}, 500);
		return false;
	});
	
	$('.gallery-page').each(function() { // the containers for all your galleries
		$(this).magnificPopup({
			delegate: 'div a',
			type: 'image',
			tLoading: 'Loading image #%curr%...',
			mainClass: 'mfp-img-mobile',
			gallery: {
				enabled: true,
				navigateByImgClick: true,
				preload: [0,1] // Will preload 0 - before current, and 1 after the current image
			},
			image: {
				tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
				titleSrc: function(item) {
					return item.el.attr('title');
				}
			}
		});
	}); 
	
	
	//FORM TOGGLE
	$('#reset-password-toggle').click(function() {
		$('#reset-password').slideToggle(500);
	});
});

// FADEOUT OF A SUCCESS REPORT [start]
setTimeout(function(){
    $('#success_report').fadeOut("slow");
}, 2000);
// FADEOUT OF A SUCCESS REPORT [end]



//DELETE CONVERSATION
function del_conversation(pnID){
	if(confirm("Bist du dir sicher, das du diese Konversation entfernen möchtest?")){
		$.ajax({
			type: "POST",
			url: "assets/js/delete_conversation.php",
			data: {
				pnID: pnID
			},
			success: function(){
				$.amaran({
				    content:{
				    	title:'Die Konversation wurde entfernt.',
				        message:'',
				        info:'Schade :(',
				        icon:'fa fa-check'
				    },
				    theme:'awesome ok'
				});
				$('#conversation_'+pnID).remove();
			},
			error: function(){
				alert("Es ist ein Fehler beim Bearbeiten der Anfrage aufgetreten.\nBitte kontaktieren Sie den Administrator.\n\nFehlercode: 278A82C8");
			}
		});
	}
}


// SWITCH
// NOTI EMAIL NEWVIDEO [start]
$('input[name="noti_email_newvideo"]').on('switchChange.bootstrapSwitch', function(event, state) {
	$.ajax({
		type: "POST",
		url: "assets/js/bootstrap-switch/noti_email_newvideo.php",
		data: {
			noti_email_newvideo: state
		},
		success: function(){
			$.amaran({
			    content:{
			    	title:'eMail Benachrichtigung für "neue Videos" wurde geändert.',
			        message:'',
			        info:'Wunderbar :)',
			        icon:'fa fa-check'
			    },
			    theme:'awesome ok'
			});
		},
		error: function(){
			alert("Es ist ein Fehler beim Bearbeiten der Anfrage aufgetreten.\nBitte kontaktieren Sie den Administrator.");
		}
	});
});
// NOTI EMAIL NEWVIDEO [end]
// SECRET PROFILE IMGS [start]
$('input[name="secret_profile_imgs"]').on('switchChange.bootstrapSwitch', function(event, state) {
	$.ajax({
		type: "POST",
		url: "assets/js/bootstrap-switch/secret_profile_imgs.php",
		data: {
			secret_profile_imgs: state
		},
		success: function(){
			$.amaran({
			    content:{
			    	title:'Offenlegung der eigenen Profilbilder wurde geändert.',
			        message:'',
			        info:'Wunderbar :)',
			        icon:'fa fa-check'
			    },
			    theme:'awesome ok'
			});
		},
		error: function(){
			alert("Es ist ein Fehler beim Bearbeiten der Anfrage aufgetreten.\nBitte kontaktieren Sie den Administrator.");
		}
	});
});
// SECRET PROFILE IMGS [end]
// NOTI NEW PN [start]
$('input[name="noti_new_pn"]').on('switchChange.bootstrapSwitch', function(event, state) {
	$.ajax({
		type: "POST",
		url: "assets/js/bootstrap-switch/noti_new_pn.php",
		data: {
			noti_new_pn: state
		},
		success: function(){
			$.amaran({
			    content:{
			    	title:'eMail Benachrichtigung für "neue Private Nachrichten" wurde geändert.',
			        message:'',
			        info:'Wunderbar :)',
			        icon:'fa fa-check'
			    },
			    theme:'awesome ok'
			});
		},
		error: function(){
			alert("Es ist ein Fehler beim Bearbeiten der Anfrage aufgetreten.\nBitte kontaktieren Sie den Administrator.");
		}
	});
});
// NOTI NEW PN [end]


// TOOLTIP
$(function () {
  $('[data-toggle="tooltip"]').tooltip()
});