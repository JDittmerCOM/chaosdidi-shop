var ITEMS_PER_PAGE = 5;

function pageselectCallback(page_index, jq){
    var new_content = $("#hiddenresult article.result").slice(page_index * ITEMS_PER_PAGE, (page_index + 1) * ITEMS_PER_PAGE).clone();
    $("#Searchresult").html(new_content);
    return false;
}

function initPagination() {
    var num_entries = $("#hiddenresult article.result").length;
    $("#Pagination").pagination(num_entries, {
        num_edge_entries: 1,
        num_display_entries: 7,
        callback: pageselectCallback,
        items_per_page: ITEMS_PER_PAGE,
        current_page: 0,
        next_text: "<i class='fa fa-angle-right'></i>",
        next_show_always: false,
        prev_text: "<i class='fa fa-angle-left'></i>",
        prev_show_always: true,
        ellipse_text: "...",
        load_first_page: true,
        show_if_single_page:false
    });
}

$(document).ready(function(){
    initPagination();
}); 