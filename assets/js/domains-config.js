function searchFor(domain_name, domain_tld, domain_id){
    var xmlHttp = null;
    // Mozilla, Opera, Safari sowie Internet Explorer 7
    if (typeof XMLHttpRequest != 'undefined') {
        xmlHttp = new XMLHttpRequest();
    }
    if (!xmlHttp) {
        // Internet Explorer 6 und älter
        try {
            xmlHttp  = new ActiveXObject("Msxml2.XMLHTTP");
        } catch(e) {
            try {
                xmlHttp  = new ActiveXObject("Microsoft.XMLHTTP");
            } catch(e) {
                xmlHttp  = null;
            }
        }
    }
    // Wenn das Objekt erfolgreich erzeugt wurde            
    if (xmlHttp) {
        $('#domainCheckResult' + domain_id).html("");
        
    	$('#domainCheckLoading' + domain_id).html("\
    		<center>\
    			<h3><strong><i class='fa fa-spinner fa-pulse fa-spin'></i> Bitte warten...</strong></h3>\
    		</center>\
    		<br><br>\
    	");

        var url = "assets/js/domains-check.php";
        var params = "domain_name="+domain_name+"&domain_tld="+domain_tld;
        
        xmlHttp.open("POST", url, true);
        
        //Headerinformationen für den POST Request
        xmlHttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xmlHttp.setRequestHeader("Content-length", params.length);
        xmlHttp.setRequestHeader("Connection", "close");                    
    
        xmlHttp.onreadystatechange = function () {
            if (xmlHttp.readyState == 4) {
                // Zurückgeliefertes Ergebnis wird in den DIV "ergebnis" geschrieben
                document.getElementById("domainCheckResult" + domain_id).innerHTML = xmlHttp.responseText;
                $('#domainCheckLoading' + domain_id).html(""); // Loading entfernen
            }
        };              
        xmlHttp.send(params);
    }else{
    	$('#domainCheckLoading' + domain_id).html("<center><strong><h3><i class='fa fa-triangle'></i> Error</h3></strong></center><br>");
    }
}