<?
	if(strlen(ID) != '0'){
		//REGISTERED DAYS [start]
		$registered_sql = mysqli_query($db, "SELECT * FROM users WHERE id = '".ID."'");
		$registered_row = mysqli_fetch_assoc($registered_sql);

		function seDay($begin,$end,$format,$sep){
			$pos1 = strpos($format, 'd');
			$pos2 = strpos($format, 'm');
			$pos3 = strpos($format, 'Y');

			$begin = explode($sep,$begin);
			$end = explode($sep,$end);

			$first = GregorianToJD($end[$pos2],$end[$pos1],$end[$pos3]);
			$second = GregorianToJD($begin[$pos2],$begin[$pos1],$begin[$pos3]);

			if($first > $second)
			  	return $first - $second;
			else
			  	return $second - $first;
		}

		$member_since = seDay(date("d.m.Y", $registered_row['date']), date("d.m.Y"), "dmY", ".");

		if($member_since >= '0'){
			$uan = ID."_0-days";
			$award_name = "0-days";
			mysqli_query($db, "INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($member_since >= '1'){
			$uan = ID."_1-day";
			$award_name = "1-day";
			mysqli_query($db, "INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($member_since >= '7'){
			$uan = ID."_1-week";
			$award_name = "1-week";
			mysqli_query($db, "INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($member_since >= '30'){
			$uan = ID."_1-month";
			$award_name = "1-month";
			mysqli_query($db, "INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($member_since >= '182'){
			$uan = ID."_6-months";
			$award_name = "6-months";
			mysqli_query($db, "INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($member_since >= '365'){
			$uan = ID."_1-year";
			$award_name = "1-year";
			mysqli_query($db, "INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($member_since >= '730'){
			$uan = ID."_2-years";
			$award_name = "2-years";
			mysqli_query($db, "INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($member_since >= '1095'){
			$uan = ID."_3-years";
			$award_name = "3-years";
			mysqli_query($db, "INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($member_since >= '1825'){
			$uan = ID."_5-years";
			$award_name = "5-years";
			mysqli_query($db, "INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		//REGISTERED DAYS [end]



		//10 AWARDS [start]
		$awards = mysqli_num_rows(mysqli_query($db, "SELECT * FROM users_awards WHERE userID = '".ID."'"));

		if($awards >= 10){
			$uan = ID."_10-badges";
			$award_name = "10-badges";
			mysqli_query($db, "INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		//10 AWARDS [end]



		//BALANCE [start]
		$balance = mysqli_query($db, "SELECT * FROM users WHERE id = '".ID."'");
		$balance_row = mysqli_fetch_assoc($balance);

		if($balance_row['credits_total'] >= 10){
			$uan = ID."_10-balance";
			$award_name = "10-balance";
			mysqli_query($db, "INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($balance_row['credits_total'] >= 50){
			$uan = ID."_50-balance";
			$award_name = "50-balance";
			mysqli_query($db, "INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($balance_row['credits_total'] >= 100){
			$uan = ID."_100-balance";
			$award_name = "100-balance";
			mysqli_query($db, "INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($balance_row['credits_total'] >= 500){
			$uan = ID."_500-balance";
			$award_name = "500-balance";
			mysqli_query($db, "INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($balance_row['credits_total'] >= 1000){
			$uan = ID."_1k-balance";
			$award_name = "1k-balance";
			mysqli_query($db, "INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($balance_row['credits_total'] >= 5000){
			$uan = ID."_5k-balance";
			$award_name = "5k-balance";
			mysqli_query($db, "INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($balance_row['credits_total'] >= 10000){
			$uan = ID."_10k-balance";
			$award_name = "10k-balance";
			mysqli_query($db, "INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($balance_row['credits_total'] >= 50000){
			$uan = ID."_50k-balance";
			$award_name = "50k-balance";
			mysqli_query($db, "INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($balance_row['credits_total'] >= 100000){
			$uan = ID."_100k-balance";
			$award_name = "100k-balance";
			mysqli_query($db, "INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($balance_row['credits_total'] >= 250000){
			$uan = ID."_250k-balance";
			$award_name = "250k-balance";
			mysqli_query($db, "INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		//BALANCE [end]



		//GOOD IDEA [start]
		$good_idea = mysqli_fetch_assoc(mysqli_query($db, "SELECT SUM(good) AS good FROM wishbox WHERE username = '".USERNAME."'"));
		
		if($good_idea['good'] >= 1){
			$uan = ID."_1-good-idea";
			$award_name = "1-good-idea";
			mysqli_query($db, "INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($good_idea['good'] >= 5){
			$uan = ID."_5-good-idea";
			$award_name = "5-good-idea";
			mysqli_query($db, "INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($good_idea['good'] >= 10){
			$uan = ID."_10-good-idea";
			$award_name = "10-good-idea";
			mysqli_query($db, "INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($good_idea['good'] >= 50){
			$uan = ID."_50-good-idea";
			$award_name = "50-good-idea";
			mysqli_query($db, "INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($good_idea['good'] >= 100){
			$uan = ID."_100-good-idea";
			$award_name = "100-good-idea";
			mysqli_query($db, "INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($good_idea['good'] >= 500){
			$uan = ID."_500-good-idea";
			$award_name = "500-good-idea";
			mysqli_query($db, "INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($good_idea['good'] >= 1000){
			$uan = ID."_1k-good-idea";
			$award_name = "1k-good-idea";
			mysqli_query($db, "INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($good_idea['good'] >= 10000){
			$uan = ID."_10k-good-idea";
			$award_name = "10k-good-idea";
			mysqli_query($db, "INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		if($good_idea['good'] >= 100000){
			$uan = ID."_100k-good-idea";
			$award_name = "100k-good-idea";
			mysqli_query($db, "INSERT INTO users_awards (userID, uan, award_name) VALUES ('".ID."', '".$uan."', '".$award_name."')");
		}
		//GOOD IDEA [end]
	}
?>